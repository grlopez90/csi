<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * About Controller
 *
 *
 * @method \App\Model\Entity\About[] paginate($object = null, array $settings = [])
 */
class AboutController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
      
    }    
    
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        // Allow users to register and logout.
        // You should not add the "login" action to allow list. Doing so would
        // cause problems with normal functioning of AuthComponent.
        $this->Auth->allow(['index']);
    }
}
