<section class="GeneralInfoContent">
    <div class="GeneralInfoContent-Banner" style="background-image:url(/img/cpf.jpg)">        
        <div class="GeneralInfoContent-Banner-TitleBox">
            <div class="GeneralInfoContent-Banner-Title">
                <h1 class="bold-text">NUESTRA AMPLIA <br> GAMA DE SERVICIOS...</h1>
            </div>

            <div class="GeneralInfoContent-Banner-SubtitleBox">
                <h2 class="GeneralInfoContent-Banner-Subtitle">
                    ...NOS HACEN LA AGENCIA DE SEGURIDAD PRIVADA MAS INTEGRAL DEL MERCADO
                </h2>
            </div>              
        </div>        
    </div>   
    <div class="GeneralInfoContent-StripeExperts">        
        <div class="container">
            <p class="GeneralInfoContent-StripeExperts-text">
                Contamos con mas de 500 empleados que son nuestra fuerza motriz. Gran parte
                de nuestro éxito se debe al excelente trabajo de este equipo humano que forma
                CSI, nuestros guardas, que día tras día se ocupan de proteger viviendas, oficinas,
                centros comerciales, fábrias e industrias, facilitando siempre la buena marcha de los
                acontecimientos.
            </p>
        </div>               
    </div>
    <div class="GeneralInfoContent-WhiteArea">
        <div class="Expertise">
            <div class="container">            
                <div class="row">
                    <div class="col-md-4 col-xs-12 col-sm-12">
                        <div class="Expertise-InfoBox">
                            <img src="/img/guard.png" class="Expertise-InfoBox-icon" alt="">
                            <h4 class="Expertise-InfoBox-title">SEGURIDAD<br>FISICA</h4>                        
                        </div>                        
                        <p class="Expertise-Text">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam ex, animi ipsam fugiat quis laborum libero maxime quod iusto id dolor error tempora quia fugit neque quam vero beatae eveniet!
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam ex, animi ipsam fugiat quis laborum libero maxime quod iusto id dolor error tempora quia fugit neque quam vero beatae eveniet!
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam ex, animi ipsam fugiat quis laborum libero maxime quod iusto id dolor error tempora quia fugit neque quam vero beatae eveniet!
                        </p>                        
                        <div class="Expertise-Icons">
                            <a class="Expertise-Icons-Button" href="#">Ver cada servicio</a>
                                <div class="Expertise-Icons-PicRow">
                                    <img src="/img/patrulla.png" alt="Escolta Vehicular" class="Expertise-Icons-Pic">
                                    <img src="/img/patrulla.png" alt="Escolta Vehicular" class="Expertise-Icons-Pic">
                                </div>
                                <div class="Expertise-Icons-PicRow">
                                    <img src="/img/patrulla.png" alt="Escolta Vehicular" class="Expertise-Icons-Pic">
                                    <img src="/img/patrulla.png" alt="Escolta Vehicular" class="Expertise-Icons-Pic">
                                </div>
                                <div class="Expertise-Icons-PicRow">
                                    <img src="/img/patrulla.png" alt="Escolta Vehicular" class="Expertise-Icons-Pic">
                                    <img src="/img/patrulla.png" alt="Escolta Vehicular" class="Expertise-Icons-Pic">
                                </div>
                                <div class="Expertise-Icons-PicRow">
                                    <img src="/img/patrulla.png" alt="Escolta Vehicular" class="Expertise-Icons-Pic">
                                    <img src="/img/patrulla.png" alt="Escolta Vehicular" class="Expertise-Icons-Pic">
                                </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12 col-sm-12">
                        <div class="Expertise-InfoBox">
                            <img src="/img/guard.png" class="Expertise-InfoBox-icon" alt="">
                            <h4 class="Expertise-InfoBox-title">SEGURIDAD<br>ELECTRÓNICA</h4>                        
                        </div>
                        <p class="Expertise-Text">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam ex, animi ipsam fugiat quis laborum libero maxime quod iusto id dolor error tempora quia fugit neque quam vero beatae eveniet!
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam ex, animi ipsam fugiat quis laborum libero maxime quod iusto id dolor error tempora quia fugit neque quam vero beatae eveniet!
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam ex, animi ipsam fugiat quis laborum libero maxime quod iusto id dolor error tempora quia fugit neque quam vero beatae eveniet!
                        </p>
                        <div class="Expertise-Icons">
                            <a class="Expertise-Icons-Button" href="#">Ver cada servicio</a>
                            <div class="Expertise-Icons-PicRow">
                                <img src="/img/patrulla.png" alt="Escolta Vehicular" class="Expertise-Icons-Pic">
                                <img src="/img/patrulla.png" alt="Escolta Vehicular" class="Expertise-Icons-Pic">
                            </div>
                            <div class="Expertise-Icons-PicRow">
                                <img src="/img/patrulla.png" alt="Escolta Vehicular" class="Expertise-Icons-Pic">
                                <img src="/img/patrulla.png" alt="Escolta Vehicular" class="Expertise-Icons-Pic">
                            </div>
                            <div class="Expertise-Icons-PicRow">
                                <img src="/img/patrulla.png" alt="Escolta Vehicular" class="Expertise-Icons-Pic">
                                <img src="/img/patrulla.png" alt="Escolta Vehicular" class="Expertise-Icons-Pic">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12 col-sm-12">
                        <div class="Expertise-InfoBox">
                            <img src="/img/guard.png" class="Expertise-InfoBox-icon" alt="">
                            <h4 class="Expertise-InfoBox-title">ASESORÍA Y<br>CAPACITACIÓN</h4>
                        </div>                        
                        <p class="Expertise-Text">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam ex, animi ipsam fugiat quis laborum libero maxime quod iusto id dolor error tempora quia fugit neque quam vero beatae eveniet!
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam ex, animi ipsam fugiat quis laborum libero maxime quod iusto id dolor error tempora quia fugit neque quam vero beatae eveniet!
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam ex, animi ipsam fugiat quis laborum libero maxime quod iusto id dolor error tempora quia fugit neque quam vero beatae eveniet!
                        </p>                        
                        <div class="Expertise-Icons">
                            <a class="Expertise-Icons-Button" href="#">Ver cada servicio</a>
                            <div class="Expertise-Icons-PicRow">
                                <img src="/img/patrulla.png" alt="Escolta Vehicular" class="Expertise-Icons-Pic">
                                <img src="/img/patrulla.png" alt="Escolta Vehicular" class="Expertise-Icons-Pic">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="MoreInfo">
        <div class="white-overlay flex-container-column-center">
            <a href="#" class="MoreInfo-Testimonials">Vea lo que dicen nuestros clientes aquí</a>
            <p class="MoreInfo-Consult">O escribanos <a href="#" class="MoreInfo-Consult-Link">aqui</a> si tiene una consulta</p>
        </div>
    </div>
</section>