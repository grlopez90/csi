<section class="ArticlesContent">
    <div class="container ArticlesContent-SearchContainer">
        <div class="row ArticlesContent-Search">
            <div class="col-10 offset-1">
                <div class="input-group">
                    <input type="text"  class="form-control" placeholder="Buscar..."/>
                    <span class="input-group-addon">
                        <i class="glyphicon glyphicon-search"></i>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="container ArticlesContent-MainContainer">        
        <div class="row ArticlesContent-Item">
            <div class="col-12 col-sm-6 offset-sm-3 col-lg-4 offset-lg-0 margin-bot-standard">
                <figure class="ArticlesContent-ImageContainer">
                    <img class="ArticlesContent-ImageInternal" src="/img/article-generic.jpg" alt="Generic image">
                </figure>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-7">
                <h2 class="ArticlesContent-ArticleTitle">
                    Titulo va aqui
                </h2>
                <p class="paragraph-justified">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque maiores distinctio ut, nisi quaerat dignissimos assumenda explicabo nam unde sit, incidunt consequuntur ea nulla voluptatibus reprehenderit magnam fuga. Laboriosam, vero!
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo aliquid illum, quisquam dolorem numquam temporibus eligendi. Nam ad qui voluptatum recusandae tempora a sunt impedit sint odit.
                </p>
                <a class="ArticlesContent-ReadMore" href="#" title="Leer mas...">Leer mas...</a>
            </div>
        </div>           
        <div class="row ArticlesContent-Item">
            <div class="col-12 col-sm-6 offset-sm-3 col-lg-4 offset-lg-0 margin-bot-standard">
                <figure class="ArticlesContent-ImageContainer">
                    <img class="ArticlesContent-ImageInternal" src="/img/article-generic.jpg" alt="Generic image">
                </figure>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-7">
                <h2 class="ArticlesContent-ArticleTitle">
                    Titulo va aqui
                </h2>
                <p class="paragraph-justified">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque maiores distinctio ut, nisi quaerat dignissimos assumenda explicabo nam unde sit, incidunt consequuntur ea nulla voluptatibus reprehenderit magnam fuga. Laboriosam, vero!
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo aliquid illum, quisquam dolorem numquam temporibus eligendi. Nam ad qui voluptatum recusandae tempora a sunt impedit sint odit.
                </p>
                <a class="ArticlesContent-ReadMore" href="#" title="Leer mas...">Leer mas...</a>
            </div>
        </div>           
        <div class="row ArticlesContent-Item">
            <div class="col-12 col-sm-6 offset-sm-3 col-lg-4 offset-lg-0 margin-bot-standard">
                <figure class="ArticlesContent-ImageContainer">
                    <img class="ArticlesContent-ImageInternal" src="/img/article-generic.jpg" alt="Generic image">
                </figure>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-7">
                <h2 class="ArticlesContent-ArticleTitle">
                    Titulo va aqui
                </h2>
                <p class="paragraph-justified">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque maiores distinctio ut, nisi quaerat dignissimos assumenda explicabo nam unde sit, incidunt consequuntur ea nulla voluptatibus reprehenderit magnam fuga. Laboriosam, vero!
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo aliquid illum, quisquam dolorem numquam temporibus eligendi. Nam ad qui voluptatum recusandae tempora a sunt impedit sint odit.
                </p>
                <a class="ArticlesContent-ReadMore" href="#" title="Leer mas...">Leer mas...</a>
            </div>
        </div>
    </div>    
</section>