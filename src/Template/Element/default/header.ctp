<div class="container">
    <div class="row">
        <div class="col">
            <div class="logo-wrapper">
                <figure class="Header-logo">
                    <img src="/img/logo-csi.png" alt="">
                </figure>
                <p class="Header-title">
                    CORPORACIÓN DE <br> SEGURIDAD INTERNACIONAL
                </p>
            </div>
        </div>
        <div class="col">
            <nav class="MainMenu navbar navbar-expand-lg navbar-light">
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="/">INICIO <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/about">QUIENES SOMOS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/services">
                                NUESTROS SERVICIOS
                            </a>
                        </li>                       
                        <li class="nav-item">
                            <a class="nav-link" href="/#contact">
                                CONTACTENOS
                            </a>
                        </li>                       
                        <li class="nav-item">
                            <a class="nav-link" href="/articles">
                                ARTICULOS
                            </a>
                        </li>                       
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</div>