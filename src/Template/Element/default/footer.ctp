<div class="container">
    <div class="row">
        <div class="col-md-4 offset-md-1 info-box csi-copyright">
            <figure class="float-left" style="margin-right:5px">
                <img src="/img/logo-csi.png" alt="CSI Logo">
            </figure>
            <p>
                Corporación de Seguridad Internacional<br>
                y las siglas CSI son una marca registrada.<br>
                El escudo y el nombre son propiedad de CSI.<br>
                Derechos Reservados 2005
            </p>
        </div>
        <div class="col-md-2 info-box social-links">
            <ul class="u-wrapper">
                <li>Facebook</li>
                <li>Twitter</li>
                <li>Youtube</li>
            </ul>
        </div>
        <div class="col-md-2 info-box contact-info">
            <ul class="u-wrapper">
                <li>(505) 0000-0000</li>
                <li>(505) 0000-0000</li>
                <li>info@csi.com.ni</li>
            </ul>
        </div>
        <div class="col-md-3 info-box creator-info">
            <p>
                Sitio diseñado y desarrollado por</br>
                CreativoCorp.com</br>
                Managua, Nicaragua</br>
                Elaborado en 2017
            </p>
        </div>
    </div>    
</div>