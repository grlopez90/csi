<section class="ServiceContent">
    <div class="ServiceContent-Service" style="background-image:url(http://www.ccmcnet.com/wp-content/uploads/2012/08/DM-Security-Group-Shot.jpg)">
        <div class="black-overlay ServiceContent-ServiceWrapper flex-center-items">
            <div class="container">
                <div class="row">
                    <div class="col-lg-1 offset-lg-1 col-sm-4 offset-sm-4 col-6 offset-3">
                        <figure class="service-fig">
                            <img src="/img/patrulla.png" alt="Service Background" class="img-fluid">
                        </figure>                
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-5 offset-lg-1 col-sm-12">
                        <p class="ServiceContent-ServiceTitle">
                            Titulo del <br class="visible-md"> servicio
                        </p>
                    </div>
                    <div class="col-lg-6 col-sm-12">
                        <p class="ServiceContent-ServiceDesc">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo quibusdam dolore inventore quidem, ipsam, veniam vero doloribus dolorum molestias, mollitia iusto! Nisi incidunt magnam doloribus vel quam impedit? Magni, eum!</span><span>Repellat aliquam esse molestiae quo minus sit architecto accusantium aut incidunt rem officia est vitae eos dolores facere modi, quae totam doloribus dolore beatae quod quam.
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-2 offset-lg-8 col-md-4 offset-md-2 ServiceContent-Contact">
                        <figure class="ServiceContent-ContactIcon">
                            <img src="/img/telefono.png" alt="Número de teléfono" class="full-height">                            
                        </figure>
                        <p class="ServiceContent-ContactInfo">
                            Llamanos <br>
                            (505) 0000-0000
                        </p>
                    </div>

                    <div class="col-lg-2 col-md-4 ServiceContent-Contact">
                        <figure class="ServiceContent-ContactIcon">
                            <img src="/img/telefono.png" alt="Número de teléfono" class="full-height">                            
                        </figure>
                        <p class="ServiceContent-ContactInfo">
                            Correo <br>
                            info@csi.com.ni
                        </p>
                    </div>
                </div>            
            </div>
        </div>
    </div>
    <div class="ServiceContent-Service" style="background-image:url(http://www.ccmcnet.com/wp-content/uploads/2012/08/DM-Security-Group-Shot.jpg)">
        <div class="black-overlay ServiceContent-ServiceWrapper flex-center-items">
            <div class="container">
                <div class="row">
                    <div class="col-lg-1 offset-lg-1 col-sm-4 offset-sm-4 col-6 offset-3">
                        <figure class="service-fig">
                            <img src="/img/patrulla.png" alt="Service Background" class="img-fluid">
                        </figure>                
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-5 offset-lg-1 col-sm-12">
                        <p class="ServiceContent-ServiceTitle">
                            Titulo del <br class="visible-md"> servicio
                        </p>
                    </div>
                    <div class="col-lg-6 col-sm-12">
                        <p class="ServiceContent-ServiceDesc">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo quibusdam dolore inventore quidem, ipsam, veniam vero doloribus dolorum molestias, mollitia iusto! Nisi incidunt magnam doloribus vel quam impedit? Magni, eum!</span><span>Repellat aliquam esse molestiae quo minus sit architecto accusantium aut incidunt rem officia est vitae eos dolores facere modi, quae totam doloribus dolore beatae quod quam.
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-2 offset-lg-8 col-md-4 offset-md-2 ServiceContent-Contact">
                        <figure class="ServiceContent-ContactIcon">
                            <img src="/img/telefono.png" alt="Número de teléfono" class="full-height">                            
                        </figure>
                        <p class="ServiceContent-ContactInfo">
                            Llamanos <br>
                            (505) 0000-0000
                        </p>
                    </div>

                    <div class="col-lg-2 col-md-4 ServiceContent-Contact">
                        <figure class="ServiceContent-ContactIcon">
                            <img src="/img/telefono.png" alt="Número de teléfono" class="full-height">                            
                        </figure>
                        <p class="ServiceContent-ContactInfo">
                            Correo <br>
                            info@csi.com.ni
                        </p>
                    </div>
                </div>            
            </div>
        </div>
    </div>
    <div class="ServiceContent-Service" style="background-image:url(http://www.ccmcnet.com/wp-content/uploads/2012/08/DM-Security-Group-Shot.jpg)">
        <div class="black-overlay ServiceContent-ServiceWrapper flex-center-items">
            <div class="container">
                <div class="row">
                    <div class="col-lg-1 offset-lg-1 col-sm-4 offset-sm-4 col-6 offset-3">
                        <figure class="service-fig">
                            <img src="/img/patrulla.png" alt="Service Background" class="img-fluid">
                        </figure>                
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-5 offset-lg-1 col-sm-12">
                        <p class="ServiceContent-ServiceTitle">
                            Titulo del <br class="visible-md"> servicio
                        </p>
                    </div>
                    <div class="col-lg-6 col-sm-12">
                        <p class="ServiceContent-ServiceDesc">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo quibusdam dolore inventore quidem, ipsam, veniam vero doloribus dolorum molestias, mollitia iusto! Nisi incidunt magnam doloribus vel quam impedit? Magni, eum!</span><span>Repellat aliquam esse molestiae quo minus sit architecto accusantium aut incidunt rem officia est vitae eos dolores facere modi, quae totam doloribus dolore beatae quod quam.
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-2 offset-lg-8 col-md-4 offset-md-2 ServiceContent-Contact">
                        <figure class="ServiceContent-ContactIcon">
                            <img src="/img/telefono.png" alt="Número de teléfono" class="full-height">                            
                        </figure>
                        <p class="ServiceContent-ContactInfo">
                            Llamanos <br>
                            (505) 0000-0000
                        </p>
                    </div>

                    <div class="col-lg-2 col-md-4 ServiceContent-Contact">
                        <figure class="ServiceContent-ContactIcon">
                            <img src="/img/telefono.png" alt="Número de teléfono" class="full-height">                            
                        </figure>
                        <p class="ServiceContent-ContactInfo">
                            Correo <br>
                            info@csi.com.ni
                        </p>
                    </div>
                </div>            
            </div>
        </div>
    </div>
</section>