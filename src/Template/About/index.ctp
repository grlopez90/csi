<section class="AboutContent">
    <div class="AboutContent-Banner" style="background-image:url(/img/banners/IMG_4537.png)">        
        <div class="AboutContent-Banner-Title">
            <h1 class="AboutContent-Banner-Title-Main">
                Ya nos conoces, <br>
                somos los amigos <br>
                que cuidamos de <br>
                los amigos
            </h1>            
        </div>        
    </div>
    <div class="AboutContent-BlackStripe">        
        <p class="AboutContent-BlackStripe-Text">
            CSI se ha consolidado como una empresa<br> líder del sector de la seguridad privada
        </p>        
    </div>
    <div class="AboutContent-Description">
        <div class="container">
            <p class="paragraph-standard">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum facere iusto magni, quidem. Qui perspiciatis voluptates cupiditate sed sunt incidunt? Aut quia itaque incidunt eos, sit debitis nemo accusantium, ipsa.
            Voluptates ad, delectus ipsum sequi, ea harum, ullam tempora est alias perferendis ipsam similique debitis sit porro facere iure, repellendus. Sint qui veniam mollitia, ipsa temporibus debitis unde voluptate obcaecati.
            Vero commodi, omnis facere vitae nam, quae atque. Expedita culpa omnis ad sed delectus, eius corrupti veniam laborum itaque magni eaque recusandae aliquam, fugiat, earum illo veritatis quae excepturi exercitationem!</p>

            <p class="paragraph-standard">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum facere iusto magni, quidem. Qui perspiciatis voluptates cupiditate sed sunt incidunt? Aut quia itaque incidunt eos, sit debitis nemo accusantium, ipsa.
            Voluptates ad, delectus ipsum sequi, ea harum, ullam tempora est alias perferendis ipsam similique debitis sit porro facere iure, repellendus. Sint qui veniam mollitia, ipsa temporibus debitis unde voluptate obcaecati.
            Vero commodi, omnis facere vitae nam, quae atque. Expedita culpa omnis ad sed delectus, eius corrupti veniam laborum itaque magni eaque recusandae aliquam, fugiat, earum illo veritatis quae excepturi exercitationem!</p>

            <p class="paragraph-standard">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum facere iusto magni, quidem. Qui perspiciatis voluptates cupiditate sed sunt incidunt? Aut quia itaque incidunt eos, sit debitis nemo accusantium, ipsa.
            Voluptates ad, delectus ipsum sequi, ea harum, ullam tempora est alias perferendis ipsam similique debitis sit porro facere iure, repellendus. Sint qui veniam mollitia, ipsa temporibus debitis unde voluptate obcaecati.
            Vero commodi, omnis facere vitae nam, quae atque. Expedita culpa omnis ad sed delectus, eius corrupti veniam laborum itaque magni eaque recusandae aliquam, fugiat, earum illo veritatis quae excepturi exercitationem!</p>            
        </div>
    </div>
    <div class="AboutContent-OurValues">
        <div class="container">
            <h2 class="area-title-yellow">Nuestros Valores</h2>
            <p class="paragraph-standard">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut, totam perspiciatis, dolore et assumenda, reprehenderit, veritatis eveniet unde ex aliquam perferendis cupiditate voluptate fuga. Mollitia deserunt, tenetur neque dolore praesentium!
            </p>
            <ul class="u-wrapper-clear">
                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque, suscipit impedit, iure vero nisi cupiditate assumenda quasi, porro reprehenderit incidunt fugiat corrupti. Facere tempora iure suscipit, quisquam ratione commodi tenetur.</li>
                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus maxime est saepe dolor eius eos numquam dolorum, officiis officia nostrum sint sapiente quasi doloremque, ut quod maiores at quibusdam neque.</li>
                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis vero doloribus facere voluptas laboriosam quis voluptate saepe itaque maxime, voluptatum, nobis cupiditate magnam corrupti cumque architecto eius adipisci facilis incidunt?</li>
            </ul>
        </div>
    </div>
    <div class="AboutContent-BottomBanner"> 
        <div class="white-overlay flex-center-items">            
            <a href="#" class="standard-button standard-button-shadow" title="Descubre todo lo que tenemos para ofrecer">
                Descubre todo lo que tenemos para ofrecer
            </a>
            <p class="AboutContent-BottomBanner-Text">O ESCRIBANOS <a class="inline-link" href="#">AQUI</a> SI TIENE UNA CONSULTA</p>                            
        </div>
    </div>
</section>