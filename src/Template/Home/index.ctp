<section class="HomeContent">
<div class="HomeContent-Banner" style="background-image:url(/img/banners/IMG_4485.png)">        
    <div class="HomeContent-Banner-Title">
        <h1>SOMOS <br> SEGURIDAD TOTAL</h1>
        <h5>NUESTRA EXPERIENCIA ESTA A SU DISPOSICION</h4>
    </div>        
    <div class="HomeContent-Expertise">
        <div class="container">            
            <div class="row">
                <div class="col-md-4 col-xs-12 col-sm-12">
                    <div class="HomeContent-InfoBox">
                        <img src="/img/services/GUARD.png" alt="">
                        <h4 class="HomeInfoBox-title">SEGURIDAD<br>FISICA</h4>
                        <a href="#" class="btn-read btn-centered">LEER MAS</a>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12 col-sm-12">
                    <div class="HomeContent-InfoBox">
                        <img src="/img/services/calc.png" alt="">
                        <h4 class="HomeInfoBox-title">SEGURIDAD<br>ELECTRÓNICA</h4>
                        <a href="#" class="btn-read btn-centered">LEER MAS</a>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12 col-sm-12">
                    <div class="HomeContent-InfoBox">
                        <img src="/img/services/gun.png" alt="">
                        <h4 class="HomeInfoBox-title">ASESORÍA Y<br>CAPACITACIÓN</h4>
                        <a href="#" class="btn-read btn-centered">LEER MAS</a>
                    </div>            
                </div>
            </div>
        </div>
    </div>
</div>
<div class="StripeExperts">
    <p class="StripeExperts-text">
        Expertos en soluciones de seguridad integral y <br>
        una amplia plataforma de servicios...
    </p>
</div>
</section>

<div class="About">
    <div class="container">
        <h2 class="title title-stand-out">¿PORQUE NOSTROS?</h2>
        <div class="row">
            <div class="col-md-4">
                <figure class="About-logo">
                    <img src="/img/logo-big.png" class="img-fluid" alt="">
                </figure>
            </div>
            <div class="col-md-8">
                <ul class="u-wrapper">
                    <li class="About-item">
                        Comunicación agil y eficiente con los Clientes
                    </li>
                    <li class="About-item">
                        Nuestros Clientes son nuestros principales activos
                    </li>
                    <li class="About-item">
                        Eficiente supervición
                    </li>
                    <li class="About-item">
                        Periódica revisión y elaboración de Diagnóstico de vulnerabilidad a la seguridad de nuestros clientes
                    </li>
                    <li class="About-item">
                        Satisfacción a nuestro personal
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="OurClients">
    <h2 class="title title-stand-out">¿QUE DICEN NUESTROS CLIENTES?</h2>
    <div class="container OurClients-container">
        <div class="row">
            <div class="col-md-4">
                <div class="Client">
                    <div class="Client-image">
                        <img src="/img/clients/yugo.png" alt="img-fluid" class="">
                    </div>
                    <div class="Client-description">
                        <p>CSI ES LA MEJOR OPCIÓN, ESTAMOS MUY CONTENTOS DE TRABAJAR CON ELLOS</p>
                    </div>    
                </div>
            </div>
            <div class="col-md-4">
                <div class="Client">
                    <div class="Client-image">
                        <img src="/img/clients/aldo.png" alt="img-fluid" class="">
                    </div>
                    <div class="Client-description">
                        <p>TENEMOS 10 AÑOS DE TRABAJAR CON CSI, SON MUY PROFESIONALES</p>
                    </div>    
                </div>
            </div>
            <div class="col-md-4">
                <div class="Client">
                    <div class="Client-image">
                        <img src="/img/clients/moncada.png" alt="img-fluid" class="">
                    </div>
                    <div class="Client-description">
                        <p>DESPUES DE UNA DECADA DE TRABAJAR CON CSI, NOS SENTIMOS ORGULLOS DE TRABAJAR CON CSI</p>
                    </div>    
                </div>
            </div>
        </div>
    </div>
    <div class="flex-center-items">            
        <a href="#" class="standard-button standard-button-shadow" title="Descubre todo lo que tenemos para ofrecer">
            Descubre todo lo que tenemos para ofrecer
        </a>
        <p class="AboutContent-BottomBanner-Text">O ESCRIBANOS <a class="inline-link" href="#">AQUI</a> SI TIENE UNA CONSULTA</p>                            
    </div>
</section>

<div class="contact" id="contact">
    <div class="container contact-list">
        <div class="row">
            <div class="col-md-4">
                <div class="contactAdv">
                    <div class="contactAdv-image">
                        <img src="/img/phone.png" alt="">
                    </div>
                    <div class="contactAdv-title">
                        LLAMANOS (505) 0000-0000
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="contactAdv">
                    <div class="contactAdv-image">
                        <img src="/img/mundo.png" alt="">
                    </div>
                    <div class="contactAdv-title">
                        DIRECCIÓN Managua, Nic
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="contactAdv">
                    <div class="contactAdv-image">
                        <img src="/img/correo.png" alt="">
                    </div>
                    <div class="contactAdv-title">
                        CORREO info@csi.com.ni
                    </div>
                </div>
            </div>
        </div>
    </div>
    <form action="">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <input type="text" class="form-control" placeholder="Your Name">
                    <input type="text" class="form-control" placeholder="Your Email">
                    <input type="text" class="form-control" placeholder="Yor Website">
                    <input type="text" class="form-control" placeholder="Yor Company">
                </div>
                <div class="col-md-6">
                    <textarea name="" id="" class="form-control" cols="30" rows="7" placeholder="Yor Comment"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <a href="#" class="standard-button standard-button-shadow float-right" title="Descubre todo lo que tenemos para ofrecer" style="margin-top:20px">
                        ENVIAR MENSAJE
                    </a>
                </div>
            </div>
        </div>
    </form>
</div>

<div class="Map">
    
</div>